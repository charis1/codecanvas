using System.Text.Json.Serialization;
using CodeCanvas.Database;
using CodeCanvas.ExchangeRateStrategies;
using CodeCanvas.HostedServices;
using CodeCanvas.Services;
using EuropeanCentralBank;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CodeCanvas
{
	public partial class Startup
	{
		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllers().AddJsonOptions(x =>
				x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve);

			services.AddSwaggerDocument();

			services.AddDbContext<ApplicationDbContext>(options =>
			{
				var connectionString = Configuration.GetConnectionString(Constants.Databases.Application);
				options.UseSqlite(connectionString);
			});
			
			// todo: register EuropeanCentralBankClient
			services.AddEuropeanCentralBank(Configuration.GetSection("europeanCentralBankSettings"));
			services.AddTransient<ICurrencyRateService, CurrencyRateService>();
			services.AddTransient<IWalletAdjustmentService, WalletAdjustmentService>();
			services.AddTransient<SpecificDateExchangeRateStrategy, SpecificDateExchangeRateStrategy>();
			services.AddTransient<SpecificDateOrNextAvailableRateStrategy, SpecificDateOrNextAvailableRateStrategy>();
			
			// todo: register UpdateRatesHostedService
			
			//Setting up expired assets through contracts renewal cron job
			services.AddHostedService<UpdateRatesHostedService>();
		}
	}
}
