using System;
using System.Linq;
using System.Threading.Tasks;
using CodeCanvas.Services;

namespace CodeCanvas.ExchangeRateStrategies
{
    public class SpecificDateExchangeRateStrategy : ExchangeRateStrategyBase
    {
        private readonly ICurrencyRateService _currencyRateService; 
            
        public SpecificDateExchangeRateStrategy(ICurrencyRateService currencyRateService)
        {
            _currencyRateService = currencyRateService;
        }
        
        protected override async Task<decimal> GetRate(string currencyCodeFrom, string currencyCodeTo, DateTime date)
        {
            var currencyRates = await _currencyRateService.GetCurrenciesRatesAsync(new DateTime(date.Year, date.Month, date.Day, 0, 0, 0));

            var fromRate = currencyRates.FirstOrDefault(cr => cr.CurrencyCode == currencyCodeFrom)?.Rate;
            var toRate = currencyRates.FirstOrDefault(cr => cr.CurrencyCode == currencyCodeTo)?.Rate;
            
            if (fromRate == null || toRate == null)
                throw new Exception("No rates found");

            return (decimal)(fromRate - toRate);
        }
    }
}