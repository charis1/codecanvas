using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeCanvas.Services;

namespace CodeCanvas.ExchangeRateStrategies
{
    public class SpecificDateOrNextAvailableRateStrategy : ExchangeRateStrategyBase
    {
        private readonly ICurrencyRateService _currencyRateService; 
            
        public SpecificDateOrNextAvailableRateStrategy(ICurrencyRateService currencyRateService)
        {
            _currencyRateService = currencyRateService;
        }
        
        protected override async Task<decimal> GetRate(string currencyCodeFrom, string currencyCodeTo, DateTime date)
        {
            //Create correct datetime
            var specificDate = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            var dates = new List<DateTime> { specificDate, /* Next day */ specificDate.AddDays(1) };
            
            //Order rates by "date" asc in order to get "specific date" rates otherwise "next date rates"
            var currencyRates = (await _currencyRateService.GetCurrenciesRatesAsync(dates))
                .OrderBy(item => item.CreatedAt)
                .ToList();

            var fromRate = currencyRates.FirstOrDefault(cr => cr.CurrencyCode == currencyCodeFrom)?.Rate;
            var toRate = currencyRates.FirstOrDefault(cr => cr.CurrencyCode == currencyCodeTo)?.Rate;
            
            if (fromRate != null && toRate != null) 
                return (decimal)(toRate / fromRate);

            throw new Exception("No rates found");
        }
    }
}