using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CodeCanvas.Middlewares
{
  public class LogResponse
{
    private readonly RequestDelegate _next;
    private readonly ILogger<LogResponse> _logger;
    private readonly string _endpoint;

    public LogResponse(RequestDelegate next, ILogger<LogResponse> logger, IOptions<string> options)
    {
        _next = next;
        _logger = logger;
        _endpoint = options.Value;
    }

    public async Task Invoke(HttpContext context)
    {
        //Check if request's response is supposed to be logged
        if(_endpoint != context.Request.Path.Value)
            await _next(context);

        //Copy a pointer to the original response body stream
        var originalBodyStream = context.Response.Body;

        //Create a new memory stream...
        using (var responseBody = new MemoryStream())
        {
            //...and use that for the temporary response body
            context.Response.Body = responseBody;

            //Continue down the Middleware pipeline, eventually returning to this class
            await _next(context);

            //Format the response from the server
            var response = await FormatResponse(context.Response);
            
            _logger.LogInformation($"{response}");

            //Copy the contents of the new memory stream (which contains the response) to the original stream, which is then returned to the client.
            await responseBody.CopyToAsync(originalBodyStream);
        }
    }
    
    private async Task<string> FormatResponse(HttpResponse response)
    {
        //We need to read the response stream from the beginning...
        response.Body.Seek(0, SeekOrigin.Begin);

        //...and copy it into a string
        string responseBody = await new StreamReader(response.Body).ReadToEndAsync();

        //We need to reset the reader for the response so that the client can read it.
        response.Body.Seek(0, SeekOrigin.Begin);

        //Return the string for the response, including the status code (e.g. 200, 404, 401, etc.)
        return $"Response: {responseBody}";
    }
}
  
  public class LoggingOption
  {
      public List<string> EndPoints { get; set; }
  }
}