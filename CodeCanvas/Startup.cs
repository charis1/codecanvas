using Microsoft.Extensions.Configuration;

namespace CodeCanvas
{
	public partial class Startup
	{
		public IConfiguration Configuration { get; }

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}
	}
}
