﻿using System.Threading.Tasks;
using CodeCanvas.Models;
using CodeCanvas.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CodeCanvas.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class WalletsController : ControllerBase
	{
		private readonly ILogger<WalletsController> _logger;
		private readonly IWalletAdjustmentService _walletAdjustmentService;

		public WalletsController(ILogger<WalletsController> logger, IWalletAdjustmentService walletAdjustmentService)
		{
			_walletAdjustmentService = walletAdjustmentService;
			_logger = logger;
		}

		[HttpPost]
		//[ProducesResponseType(typeof(CurrencyRateModel[]), StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(ApiResult<AdjustBalanceRsp>), StatusCodes.Status200OK)]
		public async Task<IActionResult> AdjustBalance([FromQuery] string exchangeRateStrategy, [FromBody] AdjustBalancePayload payload)
		{
			// The response of the request must be the updated wallet balance or 400 (bad request) in case there is no sufficient balance to subtract.
			
			try
			{
				var balance = await _walletAdjustmentService.AdjustBalance(exchangeRateStrategy, payload.WalletId, payload.CurrencyCode, payload.Amount);
				
				return Ok(new ApiResult<AdjustBalanceRsp>(new AdjustBalanceRsp(balance)));
			}
			catch (NoSufficientBalanceException)
			{
				return BadRequest();
			}
		}
	}
}
