﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CodeCanvas.Models;
using CodeCanvas.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace CodeCanvas.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class RatesController : ControllerBase
	{
		private readonly ILogger<RatesController> _logger;
		private readonly ICurrencyRateService _currencyRateService;

		public RatesController(ILogger<RatesController> logger, ICurrencyRateService currencyRateService)
		{
			_logger = logger;
			_currencyRateService = currencyRateService;
		}

		[HttpGet]
		//[ProducesResponseType(typeof(CurrencyRateModel[]), StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(ApiResult<GetRatesRsp>), StatusCodes.Status200OK)]
		public async Task<IActionResult> GetRates([FromQuery] DateTime date)
		{
			/*
			 TASK
			 Additionally, you need to log each request along with its response (by using theILogger<RatesController>
			 */
			var ratesFromDb = await _currencyRateService.GetCurrenciesRatesAsync(new DateTime(date.Year, date.Month, date.Day, 0, 0, 0));

			if (!ratesFromDb.Any())
				return NotFound();
			
			var rsp = new ApiResult<GetRatesRsp>(new GetRatesRsp(ratesFromDb.Select(item => new CurrencyRateModel(item.Id, item.CurrencyCode, item.Rate, item.CreatedAt)).ToList()));
			_logger.LogInformation(JsonConvert.SerializeObject(rsp));
			
			return Ok(rsp);
		}
	}
}
