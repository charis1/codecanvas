using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;

namespace CodeCanvas.Extensions
{
    public static class ApiLogging
    {
        public static IHostBuilder UseSerilogServiceLogging(this IHostBuilder builder)
        {
            return builder.UseSerilog((hostingContext, loggerConfiguration) 
                => loggerConfiguration
                    .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                    .MinimumLevel.Override("Microsoft.EntityFrameworkCore.Database.Command", LogEventLevel.Information)
                    .Enrich.FromLogContext()
                    .WriteTo.Console()
                    .WriteTo.File
                (
                    hostingContext.Configuration.GetSection("Serilog").GetValue<string>("ServicePath").Replace("{service}", "api"),
                    outputTemplate:"[{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} {CorrelationId} {Level:u3}] {Username} {Message:lj}{NewLine}{Exception}",
                    rollingInterval: RollingInterval.Day)
                );
        }
    }
}