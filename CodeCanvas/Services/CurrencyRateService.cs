using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeCanvas.Database;
using CodeCanvas.Entities;
using Microsoft.EntityFrameworkCore;

namespace CodeCanvas.Services
{
    public interface ICurrencyRateService
    {
        Task<List<CurrencyRateEntity>> GetCurrenciesRatesAsync(DateTime createdAtDate);
        Task CreateRangeAsync(List<CurrencyRateEntity> currencyRateEntities);
        Task UpdateRangeAsync(List<CurrencyRateEntity> currencyRateEntities);
        Task<List<CurrencyRateEntity>> GetCurrenciesRatesAsync(List<DateTime> createdAtDates);
        Task<CurrencyRateEntity> GetCurrencyRateAsync(string currencyCode, DateTime createdAt);
    }
    
    public class CurrencyRateService : ICurrencyRateService
    {
        private readonly ApplicationDbContext _dbContext;

        public CurrencyRateService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<CurrencyRateEntity> GetCurrencyRateAsync(string currencyCode, DateTime createdAt)
        {
            return await _dbContext.CurrencyRates.FirstOrDefaultAsync(cr => cr.CreatedAt == createdAt && cr.CurrencyCode == currencyCode);
        }

        public async Task<List<CurrencyRateEntity>> GetCurrenciesRatesAsync(DateTime createdAtDate)
        {
            return await _dbContext.CurrencyRates.Where(cr => cr.CreatedAt == createdAtDate).ToListAsync();
        }
        
        public async Task<List<CurrencyRateEntity>> GetCurrenciesRatesAsync(List<DateTime> createdAtDates)
        {
            return await _dbContext.CurrencyRates.Where(cr => createdAtDates.Contains(cr.CreatedAt)).ToListAsync();
        }
        
        public async Task CreateRangeAsync(List<CurrencyRateEntity> currencyRateEntities)
        {
            await _dbContext.CurrencyRates.AddRangeAsync(currencyRateEntities);
            await _dbContext.SaveChangesAsync();        
        }
        
        public async Task UpdateRangeAsync(List<CurrencyRateEntity> currencyRateEntities)
        {
             _dbContext.CurrencyRates.UpdateRange(currencyRateEntities);
             await _dbContext.SaveChangesAsync();
        }
    }
}