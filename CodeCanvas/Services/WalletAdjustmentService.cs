﻿using System;
using System.Threading.Tasks;
using CodeCanvas.Database;
using CodeCanvas.ExchangeRateStrategies;
using Microsoft.EntityFrameworkCore;

namespace CodeCanvas.Services
{
	public interface IWalletAdjustmentService
	{
		Task<decimal> AdjustBalance(string exchangeRateStrategy, int walletId, string currencyCode, decimal amount);
	}

	class WalletAdjustmentService : IWalletAdjustmentService
	{
		private readonly SpecificDateExchangeRateStrategy _specificDateExchangeRateStrategy;
		private readonly SpecificDateOrNextAvailableRateStrategy _specificDateOrNextAvailableRateStrategy;

		private readonly ApplicationDbContext _dbContext;

		public WalletAdjustmentService(SpecificDateExchangeRateStrategy specificDateExchangeRateStrategy, SpecificDateOrNextAvailableRateStrategy specificDateOrNextAvailableRateStrategy, ApplicationDbContext dbContext)
		{
			_specificDateExchangeRateStrategy = specificDateExchangeRateStrategy;
			_specificDateOrNextAvailableRateStrategy = specificDateOrNextAvailableRateStrategy;
			_dbContext = dbContext;
		}
		
		public async Task<decimal> AdjustBalance(string exchangeRateStrategy, int walletId, string currencyCode, decimal amount)
		{
			// choose the corresponding IExchangeRateStrategy based on exchangeRateStrategy
			// use IExchangeRateStrategy.Convert() to convert the amount into the currency of the wallet
			// bring WalletEntity and call Adjust() to adjust the balance
			// persist the changes
			// return new balance

			var wallet = await _dbContext.Wallets.FirstOrDefaultAsync(w => w.Id == walletId);

			if (wallet == null)
				throw new Exception("Wallet not found");
			
			var convertedAmount = exchangeRateStrategy == "SpecificDateExchangeRateStrategy"
				? await _specificDateExchangeRateStrategy.Convert(amount, currencyCode, wallet.CurrencyCode,
					DateTime.UtcNow)
				: await _specificDateOrNextAvailableRateStrategy.Convert(amount, currencyCode, wallet.CurrencyCode,
					DateTime.UtcNow);

			//Adjust balance - ef core tracks edited instance
			wallet.Adjust(convertedAmount);
			
			//Save changes
			await _dbContext.SaveChangesAsync();
			
			return wallet.Balance;
		}
	}
}
