namespace CodeCanvas.Models
{ 
    public class ApiResult<T>
    {
        public T Data { get;}

        public ApiResult() { }
        
        public ApiResult(T data)
        {
            Data = data;
        }
    }
}