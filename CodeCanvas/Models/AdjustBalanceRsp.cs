namespace CodeCanvas.Models
{
    public class AdjustBalanceRsp
    {
        public decimal Balance { get; set; }

        public AdjustBalanceRsp() { }
        
        public AdjustBalanceRsp(decimal balance)
        {
            Balance = balance;
        }
    }
}