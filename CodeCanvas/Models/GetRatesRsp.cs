using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeCanvas.Models
{
    public class GetRatesRsp
    {
        public DateTime? Date { get; set; }
        public List<CurrencyRateModel> Rates { get; set; }

        public GetRatesRsp() { }

        public GetRatesRsp(List<CurrencyRateModel> currencyRateEntities)
        {
            Date = currencyRateEntities.FirstOrDefault()?.CreatedAt;
            Rates = currencyRateEntities.Select(cre => new CurrencyRateModel(cre.Id, cre.CurrencyCode, cre.Rate, cre.CreatedAt)).ToList();
        }
    }
}