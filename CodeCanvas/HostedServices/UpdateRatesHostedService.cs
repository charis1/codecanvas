﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CodeCanvas.Entities;
using CodeCanvas.Services;
using EuropeanCentralBank.Clients;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CodeCanvas.HostedServices
{
	public class UpdateRatesHostedService : IHostedService, IDisposable
	{
		private readonly ILogger<UpdateRatesHostedService> _logger;
		private Timer _timer;
		private readonly IServiceScopeFactory _serviceScopeFactory;


		public UpdateRatesHostedService(ILogger<UpdateRatesHostedService> logger, IServiceScopeFactory serviceScopeFactory)
		{
			_serviceScopeFactory = serviceScopeFactory;
			_logger = logger;
		}

		public Task StartAsync(CancellationToken stoppingToken)
		{
			_logger.LogInformation("UpdateRatesHostedService running.");

			_timer = new Timer(UpdateRates, null, TimeSpan.Zero, TimeSpan.FromSeconds(5));

			return Task.CompletedTask;
		}

		private async void UpdateRates(object state)
		{
			using var scope = _serviceScopeFactory.CreateScope();
                
			var europeanCentralBankClient = scope.ServiceProvider.GetRequiredService<IEuropeanCentralBankClient>();
			var currencyRatesService = scope.ServiceProvider.GetRequiredService<ICurrencyRateService>();
			
			//Get today" datetime
			var todayDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 0, 0, 0, DateTimeKind.Utc);
			
			//Get daily "currency rates" from ecb endpoint
			var ecbCurrencyRates = await europeanCentralBankClient.GetRates();
			
			//Get "currency rates" from db
			var dbRates = await currencyRatesService.GetCurrenciesRatesAsync(todayDate);

			//Separate rates for Update process, and update their fields
			var toBeUpdated = dbRates.Where(dbRate =>
			{
				var item = ecbCurrencyRates.Rates.FirstOrDefault(ecbRate => ecbRate.CurrencyCode == dbRate.CurrencyCode);
				
				if (item == null)
					return false;
				
				dbRate.Update(item.Rate);
				return true;
			}).ToList();
			
			//Separate rates for Creation process
			var toBeCreated = ecbCurrencyRates
				.Rates
				.Where(item => dbRates.All(dbRate => dbRate.CurrencyCode == item.CurrencyCode))
				.Select(item => new CurrencyRateEntity(0, item.CurrencyCode, item.Rate,todayDate,todayDate))
				.ToList();

			if (toBeCreated.Any())
			{
				await currencyRatesService.CreateRangeAsync(toBeCreated);
				_logger.LogInformation($"Added {toBeCreated.Count} items into db");
			}

			if(toBeUpdated.Any())
			{
				await currencyRatesService.UpdateRangeAsync(toBeUpdated);
				_logger.LogInformation($"Updated {toBeUpdated.Count} items into db");
			}
			
			_logger.LogInformation("UpdateRatesHostedService rates updated.");
		}

		public Task StopAsync(CancellationToken stoppingToken)
		{
			_logger.LogInformation("UpdateRatesHostedService is stopping.");

			_timer?.Change(Timeout.Infinite, 0);

			return Task.CompletedTask;
		}

		public void Dispose()
		{
			_timer?.Dispose();
		}
	}
}
