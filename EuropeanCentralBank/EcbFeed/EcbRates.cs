using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace EuropeanCentralBank.EcbFeed
{
    // Generated via visual studio
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    [SerializableAttribute]
    [DesignerCategory("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "http://www.gesmes.org/xml/2002-08-01", IsNullable = false, ElementName = "Envelope")]
    public class EcbRates
    {
        [XmlElementAttribute("subject")]
        public string Subject { get; set; }
        public EnvelopeSender Sender { get; set; }
        [XmlElementAttribute(Namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")]
        public Cube Cube { get; set; }
    }

    [SerializableAttribute]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.gesmes.org/xml/2002-08-01")]
    public class EnvelopeSender
    {
        [XmlElementAttribute("name")]
        public string Name { get; set; }
    }

    [SerializableAttribute]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")]
    public class Cube
    {
        [XmlElementAttribute("Cube")]
        public SubCube SubCube { get; set; }
    }

    [SerializableAttribute]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")]
    public class SubCube
    {
        [XmlElementAttribute("Cube")] 
        public SubSubCube[] SubSubCubes { get; set; }
        
        [XmlAttributeAttribute("time")]
        public DateTime Time { get; set; }
    }

    [SerializableAttribute]
    [XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")]
    public class SubSubCube
    {
        [XmlAttributeAttribute("currency")] 
        public string Currency { get; set; }
        
        [XmlAttributeAttribute("rate")]
        public decimal Rate { get; set; }
    }
}