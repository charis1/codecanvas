﻿using EuropeanCentralBank.Clients;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EuropeanCentralBank
{
	public static class ServiceCollectionExtensions
	{
		public static IServiceCollection AddEuropeanCentralBank(this IServiceCollection services, IConfiguration configuration)
		{
			// todo: register EuropeanCentralBankClient and EuropeanCentralBankSettings

			// use this method to:
			// 1) register EuropeanCentralBankClient
			// 2) register the EuropeanCentralBankSettings 

			var ratesEndpoint = configuration.GetSection("ratesEndpoint").Value;
			
			services.Configure<EuropeanCentralBankSettings>(options => options.RatesEndpoint = ratesEndpoint);
			services.AddHttpClient<IEuropeanCentralBankHttpClient, EuropeanCentralBankHttpClient>();
			services.AddTransient<IEuropeanCentralBankClient, EuropeanCentralBankClient>();

			return services;
		}
	}
}
