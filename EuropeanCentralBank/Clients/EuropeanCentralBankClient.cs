﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using EuropeanCentralBank.EcbFeed;
using Microsoft.Extensions.Options;

namespace EuropeanCentralBank.Clients
{
	public interface IEuropeanCentralBankClient
	{
		Task<RatesResponse> GetRates();
	}
	
	internal class EuropeanCentralBankClient : IEuropeanCentralBankClient
	{
		private readonly EuropeanCentralBankSettings _settings;
		private readonly IEuropeanCentralBankHttpClient _europeanCentralBankHttpClient;

		public EuropeanCentralBankClient(IEuropeanCentralBankHttpClient europeanCentralBankHttpClient,  IOptions<EuropeanCentralBankSettings> settings)
		{
			_europeanCentralBankHttpClient = europeanCentralBankHttpClient;
			_settings = settings.Value;
		}

		public async Task<RatesResponse> GetRates()
		{
			// todo: implement EuropeanCentralBankClient.GetRates()

			// 1) make http call to European Central Bank (_settings.Endpoint) to get the latest rates
            // 2) parse response
			// 3) create RatesResponse
			// 4) return RatesResponse

			var request = new HttpRequestMessage(HttpMethod.Get, new Uri(_settings.RatesEndpoint));

			return new RatesResponse(await _europeanCentralBankHttpClient.SendAsync<EcbRates>(request));
		}
	}
}
