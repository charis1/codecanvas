using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace EuropeanCentralBank.Clients
{
    internal interface IEuropeanCentralBankHttpClient
    {
        Task<T> SendAsync<T>(HttpRequestMessage request, CancellationToken cancellationToken = default(CancellationToken));
    }
    
    internal class EuropeanCentralBankHttpClient : IEuropeanCentralBankHttpClient
    {
        private readonly HttpClient _httpClient;

        public EuropeanCentralBankHttpClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<T> SendAsync<T>(HttpRequestMessage request, CancellationToken cancellationToken = default(CancellationToken))
        {
            var httpResponseMessage = await _httpClient.SendAsync(request, cancellationToken);
            var content = await httpResponseMessage.Content.ReadAsStreamAsync();
            
            XmlSerializer ser = new XmlSerializer(typeof(T));
            return (T)ser.Deserialize( await httpResponseMessage.Content.ReadAsStreamAsync());
        }
    }
}