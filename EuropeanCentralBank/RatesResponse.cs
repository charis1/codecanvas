﻿using System;
using System.Collections.Generic;
using System.Linq;
using EuropeanCentralBank.EcbFeed;

namespace EuropeanCentralBank
{
	public class RatesResponse
	{
		public DateTime? Date { get; set; }
		public IEnumerable<CurrencyRate> Rates { get; set; }

		public RatesResponse() { }
		
		public RatesResponse(DateTime date, List<CurrencyRate> rates)
		{
			Date = date;
			Rates = rates.Select(item => new CurrencyRate(item.CurrencyCode, item.Rate));
		}

		public RatesResponse(EcbRates ecbRates)
		{
			Date = ecbRates.Cube?.SubCube?.Time;

			Rates = ecbRates
				.Cube?
				.SubCube?
				.SubSubCubes?.Select(item => new CurrencyRate(item.Currency, item.Rate))
				.ToList();
		}
	}
}
