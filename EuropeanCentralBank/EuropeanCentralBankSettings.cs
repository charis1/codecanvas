﻿#nullable disable
namespace EuropeanCentralBank
{
	internal class EuropeanCentralBankSettings
	{
		public string RatesEndpoint { get; set; }
	}
}
